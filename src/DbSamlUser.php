<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\saml;

use DateTime;
use ReflectionException;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\DBX;
use uhi67\uxapp\Model;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLElement;

/** @noinspection PhpUnused */

/**
 * # DbSamlUser
 *
 * SAML User data stored in database
 *
 * Recommended to override:
 * - findUser(),
 * - updateUser(),
 * - importUser()
 *
 * Usage
 * -----
 *
 * ### In the config
 *
 * ```
 * 'components' => [
 * 		'user' => [
 * 		'class' => DbSamlUser::class,
 * 		'authsource' => 'default-sp',							// id from simplesamlphp/config/authsources.php; Default is 'default-sp'
 * 		'idp' => 'https://idp...../saml2/idp/metadata.php', 	// Default for `idp` field of the user table
 * 		'org' => '...',											// name of the home organization. Default for the `org` field of the user table
 * 		'uidfield' => 'eduPersonPrincipalName',
 * 		'scope' => 'pte.hu',
 * 		'db' => $app->db,
 * 		'modelName' => 'Appuser',
 * 	],
 * 	...
 * ],
 * ```
 *
 * ### Required SAML attributes (sample only, define in the custom user class):
 * - `schacHomeOrganization` or `o` or `eduPersonScopedAffiliation` (one of them is mandatory)
 * - `uid` or attribute specified in `config/saml/uidfield` (one of them is mandatory)
 * - `eduPersonTargetedID` or `eduPersonPrincipalName` (one of them is mandatory)
 * - `mail`
 * - `displayName` or `cn` or `givenName` + `sn`
 */

class DbSamlUser extends SamlUser {
	/** @var DBX $db database connection */
	public $db;
	/** @var string $modelName -- User table modelname */
	public $modelName;

	/** @var BaseUserInstance|Model $instance -- User record instance (Model) */
	public $instance;

	/**
	 * Prepares the user component
	 *
	 * @throws
	 */
	function prepare() {
		parent::prepare();
		if(!$this->modelName) throw new UXAppException('modelName parameter is mandatory');
		if(!is_a($this->modelName, BaseUserInstance::class, true)) throw new UXAppException('modelName must be a '.BaseUserInstance::class. ', got '.$this->modelName);
		if(!is_a($this->modelName, Model::class, true)) throw new UXAppException('modelName must be a '.Model::class);
	}

	/**
	 * User::loadUser()
	 * Loads user data into session after login and updates lastclick
	 *
	 * @return void
	 * @throws
	 */
    public function loadUser() {
    	parent::loadUser(); // calls loadSession()
    	UXApp::trace(['id' => $this->id]);
		if(!$this->id) $this->getUserData();

		if($this->id) {
			$this->login($this->id);
        }
    }

	/**
	 * Finds the SAML-authenticated user in the database. Null if not found.
	 * Recommended to be overridden.
	 *
	 * The default rules are:
	 * - UID field equals uid attribute AND IDP=null or equals idp attribute;
	 * - IDP is equal AND UID@scope = uid
	 * - EMAIL = email AND UID=null AND (IDP is null or is equal to idp)
	 * @return DbSamlUser
	 */
    public function findUser() {
		if(!$this->scope) {
			$this->scope = parse_url($this->idp, PHP_URL_HOST);
			if(preg_match('~(\w+\.\w+)$~', $this->scope, $mm)) {
				$this->scope = $mm[1];
			}
		}
		$params = [$this->uid, $this->email, $this->idp, $this->scope];
		/** @see Model::first() */
		$instance = call_user_func([$this->modelName, 'first'], [
			'or',
			['uid'=>'$1', ['or', ['is null', 'idp'], 'idp'=>'$3']],
			['idp'=>'$3', ['=', ['||', 'uid', "'@'", '$4'], '$1']],
			['email'=>'$2', ['is null', 'uid'], ['or', ['is null', 'idp'], 'idp'=>'$3']]
		], [['uid', DBX::ORDER_ASC, DBX::NULLS_LAST]], $params, $this->db);
		UXApp::trace(['instance' => $instance]);
		return $instance;
	}

	/**
	 * Azonosítja a felhasználót az adatbázisban, és létrehozza, ha nincs meg.
	 *
	 * Betölti a felhasználói attributumokat a sessionbe az adatbázisból.
	 *
	 * Javasolt alkalmazásszinten felülírni.
	 *
	 * @return bool - success
	 * @throws
	 */
	public function getUserData() {
		parent::getUserData();  // Load from SAML

		// Azonosítás uid vagy email alapján
		$this->instance = $this->findUser();

		if(!$this->instance) { // Not found in database
			UXApp::trace('not found');
			$this->instance = $this->importUser();
		}
		else {
			$this->updateUser();
		}
		if($this->instance) {
			$this->enabled = $this->instance->enabled;
			$this->id = $this->instance->id;
		}
		else {
			$this->enabled = false;
			$this->id = null;
		}
		$this->saveSession();
		return $this->id;
	}

	/**
	 * Saves user data back to user table
	 * @return BaseUserInstance
	 * @throws
	 */
	function updateUser() {
		if(!$this->instance) throw new UXAppException('User instance not found');
		$this->instance->setAttributes([
			'name' => $this->name,
			'email' => $this->email,
			'uid' => $this->uid,
			'org' => $this->org,
			'logins' => $this->instance->logins + 1,
			'lastlogin' => new DateTime(),
		]);
		if(!$this->instance->idp) $this->instance->idp = $this->idp;
		$this->instance->save();
		return $this->instance;
	}

	/**
	 * Creates and saves a new user record based on current user data and auth source attributes
	 * Returns null if user creation is not supported (may be implemented in override)
	 *
	 * @return Model
	 * @throws
	 */
	function importUser() {
		// Determine name of home organization
		$edupersonScopedAffiliation = isset($attributes['eduPersonScopedAffiliation']) && $this->attributes['eduPersonScopedAffiliation'][0] ? $this->attributes['eduPersonScopedAffiliation'][0] : '';
		$org = isset($attributes['o']) ? $this->attributes['o'][0] : null;
		if(!$org && isset($attributes['schacHomeOrganization'])) $org = $this->attributes['schacHomeOrganization'][0];
		if(!$org && $edupersonScopedAffiliation) $org = preg_replace('/^[^@]*@/','', $edupersonScopedAffiliation);

		$this->instance = new $this->modelName([
			'name'=> $this->name,
			'uid' => $this->uid,
			'email' => $this->email,
			'loggedin' => 1,
			'lastlogin' => new DateTime(),
			'org' => $org,
			'idp' => $this->idp,
			'created' => new DateTime()
		]);
		if(!$this->instance->save()) {
			throw new UXAppException(
				'A felhasználó létrehozása nem sikerült. ' .
				ArrayUtils::getValue($this->instance->friendlyErrors, 0)
			);
		}

		$this->id = $this->instance->id;
		$_SESSION['user_rights'] = 0;
		$_SESSION['user_home'] = 0;
		$_SESSION['user_enabled'] = 0;
		return $this->instance;
	}

	/** @noinspection PhpUnused */

	/**
	 * Creates list of logged in users
	 *
	 * @param UXMLElement $node_parent
	 *
	 * @throws
	 */
	function createUserList($node_parent) {
		$node_userlist = $node_parent->addNode('userlist');

		/** @see Model::all() */
		/** @var array $users -- array of Person */ /** @see Person */
		$users = call_user_func([$this->modelName, 'all'], [
			'loggedin' > 0
		], [['lastclick', DBX::ORDER_DESC, DBX::NULLS_LAST]]);
		$now = new DateTime();
		Model::createNodes($node_parent, $users, [
			'nodeName' => 'user',
			'attributes' => ['id', 'name', 'lastclick'],
			'extra' => [
				'age' => function($item) use($now) {
					return $now->diff($item->lastclick);
				}
			],
		]);

		$node_userlist->setAttribute('timeout', UXApp::$app->session ? UXApp::$app->session->lifetime : 1800);
	}

	/**
	 * Logs out all overaged users
	 *
	 * @throws
	 * @noinspection PhpUnused
	 */
	public function autoLogout() {
		$model = $this->modelName;
		/** @var Model $model */
		$model::updateAll(['loggedin' => 0], ["(age(lastclick, current_timestamp) > interval '330 seconds')"]);
	}


	/**
	 * @param mixed $id -- id, uid or array of SAML attributes
	 *
	 * @return bool|BaseUserInstance
	 * @throws UXAppException
	 * @throws ReflectionException
	 */
	public function login($id) {
		if(is_array($id)) {
			foreach($id as $key=>$value) $this->$key = $value;
			$this->instance = $this->findUser();
			$this->saveSession();
			return true;
		}
		else if(is_numeric($id)) $this->instance = call_user_func([$this->modelName, 'first'], $id);
		else if(is_string($id)) $this->instance = call_user_func([$this->modelName, 'first'], ['uid'=>$id]);
		else {
			$this->instance = null;
			$this->logout();
			return true;
		}
		if($this->instance) {
			$this->instance->lastclick = new DateTime();
			$this->instance->save();
			$this->enabled = $this->instance->enabled;
			$this->loadInstance($this->instance);
			$this->saveSession();
			return $this->instance;
		}
		else {
			$this->logout();
			return false;
		}
	}

	/**
	 * Loads user instance data into AppUser (to be saved into session later)
	 * @param BaseUserInstance $instance
	 */
	public function loadInstance($instance) {
		$this->populate($instance->toArray(), ['id', 'uid', 'tid', 'idp', 'scope', 'name', 'org', 'attributes', 'jpegPhoto']);
	}

	public function getIsAuth() {
		return !!$this->id;
	}
}
