<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace uhi67\saml;

use DOMException;
use SimpleSAML\Utils\HTTP;
use SimpleSAML\Auth\Simple;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\BaseUser;
use uhi67\uxapp\Component;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLElement;

/**
 * # SamlUser - User auhtenticated by SAML authsource
 *
 * ### Required SAML attributes
 * - `schacHomeOrganization` or `o` or `eduPersonScopedAffiliation` (one of them is mandatory)
 * - `uid` or attribute specified in `uidField` (one of them is mandatory)
 * - `eduPersonTargetedID` or `eduPersonPrincipalName` (one of them is mandatory)
 * - `mail`
 * - `displayName` or `cn` or `givenName` + `sn`
 * - `jpegPhoto`
 */
class SamlUser extends BaseUser {
	// Configuration parameters
	/** @var string $authsource -- id from simplesamlphp/config/authsources.php; Default is 'default-sp' */
	public $authsource = 'default-sp';
	/** @var string $uidField */
	public $uidField = 'uid'; // or eduPersonTargetedID
	/** @var string $idp -- default idp in the configuration, current idp of the user */
	public $idp;
	/** @var string $org -- Default org name or current org name */
	public $org;
	/** @var string -- default user (attribute-) scope; scope of current user */
	public $scope;
	/** @var string $dirname -- path to preinstalled simpleSAMLphp instance */
	public $dirname;

	// Properties of the current user
	/** @var array $attributes SAML attributes */
	public $attributes;
	/** @var string $jpegPhoto -- user's photo from SAML */
	public $jpegPhoto;
	/** @var string $uid Remote user id, by config/ldap/uidfield */
	public $uid;
	/** @var string eduPersonTargetedID */
	public $tid;

	/** @var Simple $auth */
	protected $auth;

	/**
	 * SamlUser preparation
	 *
	 * @throws UXAppException
	 */
	function prepare() {
		if($this->dirname) {
			if(!file_exists($this->dirname.'/lib/_autoload.php')) {
				throw new UXAppException('SimpleSAMLphp autoload file is missing.', $this->dirname.'/lib/_autoload.php');
			}
			/** @noinspection PhpIncludeInspection */
			require $this->dirname.'/lib/_autoload.php';
			//require $this->dirname.'/lib/_autoload_modules.php';
		}
		$this->auth = new Simple($this->authsource);
    	if($this->session->get('trace')!='') ini_set('display_errors', 'stdout');
    	if(array_key_exists('login', $_REQUEST) && !$this->auth->isAuthenticated()) {
    		#echo "nincs belépve";
    		$this->auth->requireAuth();
    	}
        if($this->auth->isAuthenticated()) $this->loadUser();
		if(array_key_exists('logout', $_REQUEST)) {
			$this->id = (int)$this->session->get('user_id');
			$this->logout(); // never returns
		}
	}

	function finish() {
		parent::finish();
	}

	/**
	 * ## Loads user data from session
	 *
	 * Override for retrieving more user-related data
	 *
	 * @throws UXAppException
	 */
	function loadSession() {
		$this->id = (int)$this->session->get('user_id');
		$this->uid = $this->session->get('user_uid');
		$this->tid = $this->session->get('user_tid');
		$this->idp = $this->session->get('user_idp');
		$this->scope = $this->session->get('user_scope');
		$this->name = $this->session->get('user_name');
		$this->org = $this->session->get('user_org');
		$this->attributes = $this->session->get('user_attributes');
		$this->jpegPhoto = $this->session->get('user_jpegPhoto');
	}

	public function saveSession() {
		parent::saveSession();	// id, name, email, enabled
		$_SESSION['user_uid'] = $this->uid;
		$_SESSION['user_tid'] = $this->tid;
		$_SESSION['user_idp'] = $this->idp;
		$_SESSION['user_scope'] = $this->scope;
		$_SESSION['user_org'] = $this->org;
		$_SESSION['user_attributes'] = $this->attributes;
		$_SESSION['user_jpegPhoto'] = $this->jpegPhoto;
	}

	function loggedIn() {
		return $this->id;
	}

	/**
	 * @param Component|UXMLElement $node_parent
	 *
	 * @return UXMLElement|void
	 * @throws DOMException
	 */
	function createNode($node_parent) {
		if($node_parent instanceof Component) $node_parent = $node_parent->node;
		$attr = [];
        if($this->name) $attr['name'] = $this->name;
        if($this->id) $attr['id'] = $this->id;
        if($this->uid) $attr['uid'] = $this->uid;
        if($this->enabled) $attr['enabled'] = $this->enabled;
		return $this->node = $node_parent->addNode('user', $attr);
	}

	function logout() {
		parent::logout();
		if($this->auth->isAuthenticated()) {
			$this->auth->logout(HTTP::getSelfURLNoQuery()); /* will never return. */
		}
	}

	/**
	 * Empties user data
	 *
	 * Override for deleting more user-related data
	 */
	function emptyuser() {
		$this->id = null;
		$this->uid = null;
		$this->tid = null;
		$this->idp = null;
		$this->scope = null;
		$this->name = null;
		$this->org = null;
		$this->attributes = null;
		$this->jpegPhoto = null;
	}

	/**
	 * ## User::loadUser()
	 * Loads user data into session after login
	 *
	 * @return void
	 * @throws UXAppException
	 */
    protected function loadUser() {
        $this->loadSession();
    	$this->attributes = $this->auth->getAttributes();
    	UXApp::trace(['attributes', $this->attributes]);

		// Determine name of home organization
		$edupersonScopedAffiliation = isset($this->attributes['eduPersonScopedAffiliation']) && $this->attributes['eduPersonScopedAffiliation'][0] ? $this->attributes['eduPersonScopedAffiliation'][0] : '';
		$this->org = isset($this->attributes['o']) ? $this->attributes['o'][0] : null;
		if(!$this->org && isset($this->attributes['schacHomeOrganization'])) $this->org = $this->attributes['schacHomeOrganization'][0];
		if(!$this->org && $edupersonScopedAffiliation) $this->org = preg_replace('/^[^@]*@/','', $edupersonScopedAffiliation);

		$this->getIdp();

		$uidfield = $this->uidField;
		if(is_array($uidfield)) {
			foreach($uidfield as $uidf) {
				if(isset($this->attributes[$uidf])) {
					$uidfield = $uidf; break;
				}
			}
			if(is_array($uidfield))	throw new UXAppException(UXApp::la('uapp', 'Missing user attributes `[{$attr}]`, given `{$set}`', ['attr'=>implode(', ', $uidfield), 'set'=>Util::objtostr($this->attributes)]));
		} else {
			if(!isset($this->attributes[$uidfield])) {
				throw new UXAppException(UXApp::la('uapp', 'Missing user attribute `{$attr}`, given `{$set}`', ['attr'=>$uidfield, 'set'=>Util::objtostr($this->attributes)]));
			}
		}
		$this->uid = $this->attributes[$uidfield][0];

		// Determine scope
		$this->getScope(['schacHomeOrganization', 'eduPersonScopedAffiliation', 'eduPersonPrincipalName', 'idp']);

		/** @var array $mails */
		$mails = ArrayUtils::getValue($this->attributes, 'mail');
		$this->email = ArrayUtils::getValue($mails, 0);

    	$this->enabled = 0;
        UXApp::trace(['uid', $this->uid]);
    	if(!isset($_SESSION['user_uid']) || $this->uid != $_SESSION['user_uid']) $this->getUserData();
    }

	/**
	 * Betölti a felhasználói attributumokat SAML-ból a session-be
	 *
	 * Betölti az user rekordba:
	 *
	 * - jpegPhoto (sessionbe nem)
	 * - name (one of displayName, cn, sn, givenName)
	 *
	 * Betölti a sessionbe:
	 *
	 * - attributes
	 * - uid
	 * - org
	 * - idp
	 * - name
	 *
	 * Javasolt alkalmazásban felülírni
	 *
	 * @return bool - success
	 */
	protected function getUserData() {
		/* Override it in DbUser */
		if(isset($this->attributes['jpegPhoto'])) $this->jpegPhoto = $this->attributes['jpegPhoto'];
        unset($this->attributes['jpegPhoto']);
  		$_SESSION['attributes'] = $this->attributes;
		$_SESSION['user_uid'] = $this->uid;
		$_SESSION['user_id'] = $this->id;
		$this->name = coalesce(
			isset($this->attributes['displayName']) ? $this->attributes['displayName'][0] : null,
			isset($this->attributes['cn']) ? $this->attributes['cn'][0] : null,
			isset($this->attributes['givenName']) && isset($this->attributes['sn']) ?
				$this->attributes['givenName'][0].' '.$this->attributes['sn'][0] : null,
			'anonymous');
		$_SESSION['user_name'] = $this->name;
		$_SESSION['user_org'] = $this->org;
		$_SESSION['user_idp'] = $this->idp;
		return true;
	}

	/**
	 * Returns idp entity identifier of the IdP identified the current user
	 *
	 * @return NULL|string
	 * @noinspection PhpUndefinedClassInspection
	 * @noinspection RedundantSuppression
	 */
	function getIdp() {
		$this->idp = null;
		if(method_exists($this->auth, 'getAuthData')) {
			$this->idp = $this->auth->getAuthData('saml:sp:IdP');
			#Uapp::trace(['idp1', $this->idp);
		}
		else if(method_exists('SimpleSAML_Session', 'getInstance')) {
			// SimpleSamlPhp Older than 1.9 version
			/** @var SimpleSAML_Session $session */
			/** @noinspection PhpUndefinedMethodInspection */
			$session = SimpleSAML_Session::getInstance();
			/** @noinspection PhpUndefinedMethodInspection */
			$this->idp = $session->getIdP();
			#Uapp::trace(['idp2', $this->idp);
		}
		return $this->idp;
	}

	/**
	 * Meghatározza a felhasználó tényleges scope-ját az attributumkészletéből,
	 * figyelembe véve a megadott attributum neveket, sorrendben.
	 *
	 * Speciális attributumnév az 'idp', ekkor az azonosító idp hostneve lesz a scope.
	 *
	 * @param array $targetedAttributeNames
	 *
	 * @throws UXAppException
	 */
	private function getScope(array $targetedAttributeNames) {
		$targetedAttribute = null;
		foreach($targetedAttributeNames as $name) {
			if($name=='idp') {
				$targetedAttribute =  parse_url($this->idp, PHP_URL_HOST);
				break;
			}
			if(($value = ArrayUtils::getValue($this->attributes, $name, false))) {
				$targetedAttribute = $value;
				break;
			}
		}
		if(!$targetedAttribute) throw new UXAppException(UXApp::la('uapp', 'Missing scoped attribute.'));

		if(is_array($targetedAttribute) && isset($targetedAttribute[0])) $targetedAttribute = $targetedAttribute[0];

		#UXApp::trace(['uidvalue', $this->attributes[$uidfield]);
		$this->tid = $targetedAttribute;
		if(is_array($this->tid) && isset($this->tid['NameQualifier'])) {
			$this->scope = parse_url($this->tid['NameQualifier'], PHP_URL_HOST);
			$this->tid = $this->tid['Value'];
		}
		else {
			/** @noinspection PhpUnhandledExceptionInspection -- exception may occur only if delimiter is empty */
			$this->scope = Util::substring_after($this->tid, '@', true);
		}
	}

	public function getIsAuth() {
		return $this->uid;
	}
}

function coalesce($a) {
	if($a) return $a;
	for($i=1;$i<func_num_args();$i++){
		if($b = func_get_arg($i)) return $b;
	}
	return false;
}
