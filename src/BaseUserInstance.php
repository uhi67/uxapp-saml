<?php


namespace uhi67\saml;

/**
 * Interface BaseUserInstance
 *
 * @package uhi67\saml
 * @property int $id
 * @property int $logins
 * @property string $idp
 * @property string $uid
 * @property \DateTime $lastclick
 * @property bool $enabled
 */
Interface BaseUserInstance {
	public function toArray(array $fields = null, $recursive = false);
}
