Saml Module for UXApp
========================

v1.3.1 -- 2022-06-22

SAML-based user authentication.
Provides SamlUser and DbSamlUser classes.

This version installs own copy of SimpleSAMLphp into vendors set. 
Does not work (not recommended) with server-preinstalled SimpleSAMLphp. 

Installation
------------

Add to your `composer.json`:

    {
      "require": {
        "uhi67/uxapp-saml": "*"
      },
    }

Usage
-----

### In the config

Example

```
    'components' => [
        'user' => [
			'class' => DbSamlUser::class,
            'authsource' => 'default-sp',							// id from simplesamlphp/config/authsources.php; Default is 'default-sp'

            'idp' => 'https://idp.pte.hu/saml2/idp/metadata.php', 	// Default idp person tábla idp mezőjéhez
            'org' => 'Pécsi Tudományegyetem',						// Default org person tábla org mezőjéhez. Azonosító szervezet neve.
            'uidField' => 'eduPersonPrincipalName',
            'scope' => 'pte.hu',

            'db' => $app->db, 
            'modelName' => 'Appuser',
        ],
        ...
    ],
```

... or use any custom descendant of SamlUser.

When using descendants of DbSamlUser, it's recommended to override:

- findUser(),
- updateUser(),
- importUser(),


Change log
==========
## v1.3.1 -- 2022-06-22

- Logout on unsuccesful login (security issue)
- codeception support fix

## v1.3 -- 2021-05-13

- This version installs own copy of SimpleSAMLphp into vendors set
- saveSession() added;
- loadInstance() added;
- login(): populates from instance

## v1.2 -- 2021-05-05

- SamlUser::getScope() added
- createNode accepts any Component, returns the node
- requires UXApp 1.5.3
- DbSamlUser::login() performs instance check
- isAuth is computed propery now instead of field.

## v1.1 -- 2020-11-03

- repository is public, using packagist

## v1.0 -- 2020-06-06

